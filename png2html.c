/* png2html 1.1 - Transforms a PNG image into a web page                     *
 * Copyright (c) 1999 Geoff Holden                                           *
 *                                                                           *
 * This program is free software; you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation; either version 2 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program; if not, write to the Free Software               *
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA *
 *                                                                           *
 * The Author can be contacted by email at: holden@engr.mun.ca               */

/* Prototypes */
#include <gd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
void getcharacter(void);

/* Global Vars */
char s[6];
FILE *text;

/* Main Program */
int main(int argc, char **argv) {

/* Variables */
FILE *in, *out;
gdImagePtr im;
int c,x,y;
int r1,    g1,    b1;
int r2=-1, g2=-1, b2=-1;
int needclosefont = 0;
char code = 1;

/* Program header */
printf("------------------------------");
printf(                           "png2html 1.1");
printf(                                    "------------------------------\n");
printf("Note:\tThis program assumes that 1 pixel should equal 1 character.\n");
printf("\tBut characters are taller than they are wide, so you may want to\n");
printf("\tresize the image first.\n\n");

/* Check for errors... */
if (argc < 4) {
   fprintf(stderr, "Usage: %s imagefile.png textfile.txt output.html compatibility\n", argv[0]);
   fprintf(stderr, "\tWhere compatibility is either 0, 1 or 2\n");
   fprintf(stderr, "\t0 = Small Source, but uncompatibile with some browsers\n");
   fprintf(stderr, "\t1 = Medium Size, works with almost anything.\n");
   fprintf(stderr, "\t2 = Large Source, strict HTML.\n\n");
   exit(1);
   }; 
in = fopen(argv[1], "rb");
if (!in) {
   fprintf(stderr, "%s does not exist!\n", argv[1]);
   exit(1);
   };
if (strncmp(argv[2],"-",1)==0)
   text = stdin;
   else
   text = fopen(argv[2],"r");
if (!text) {
   fprintf(stderr, "%s does not exist!\n", argv[2]);
   exit(1);
   };
if (strncmp(argv[3],"-",1)==0)  /* If argument is "-" then output to stdout */
   out = stdout;
   else
   out = fopen(argv[3],"w");
if (!out) {
   fprintf(stderr, "%s could not be opened for writing!\n", argv[3]);
   exit(1);
   };
if (argc == 5) {
   code = atoi(&argv[4][0]);
   if ((code != 0) && (code != 1) && (code != 2))
      fprintf(stderr, "Compatibility level not recognized... using 1.\n");
   };
im = gdImageCreateFromPng(in);
fclose(in);

/* HTML Header */
fprintf(out, "<HTML>\n<HEAD>\n<TITLE>%s</TITLE>\n</HEAD>\n\n", argv[1]);
fprintf(out, "<!-- Image created with png2html by Geoff Holden  -->\n");
fprintf(out, "<!--               holden@engr.mun.ca             -->\n");
fprintf(out, "<!-- http://www.engr.mun.ca/~holden/png2html.html -->\n");
fprintf(out, "\n<BODY BGCOLOR=000000>\n<PRE><FONT SIZE=-3>");

for (y=0;y<gdImageSY(im);y++) {
   for (x=0;x<gdImageSX(im);x++) {
      c = gdImageGetPixel(im, x, y);
          r1 = im->red[c];
          g1 = im->green[c];
          b1 = im->blue[c];
          if ((r1 != r2) || (g1 != g2) || (b1 != b2)) {
             if (code != 0)
                if (needclosefont) {
                   fprintf(out, "</FONT>");
                   needclosefont = 0;
                   }; 
             if (code == 2)
                fprintf(out, "<FONT COLOR=\"#%.2X%.2X%.2X\">", r1, g1, b1);
                else
                fprintf(out, "<FONT COLOR=%.2X%.2X%.2X>", r1, g1, b1);
             r2 = r1;     /* Contitionally putting in the FONT Tag reduces */
             g2 = g1;     /* HTML size _DRASTICLY_                         */
             b2 = b1;
             needclosefont = 1;
             };
          getcharacter();
          fprintf(out, "%s", s);
      };
   fprintf(out, "\n");
   };
   fprintf(out, "</FONT></PRE>\n</BODY>\n</HTML>\n");
gdImageDestroy(im);     /* Clean up stuff */
fclose(text);
fclose(out);
return 0;
}

/* Grab a character from the text file and make sure it's valid */
void getcharacter() {
int c;
   c = getc(text);
   if (c == EOF) {
      fseek(text,0,SEEK_SET);      /* When we reach the EOF, go to beginning */
      c = getc(text);
      };
   if (c < 33) {
      getcharacter();      /* Recursivly get a new character */
      return;
      };
   if (c > 126) {
      getcharacter();      /* Recursivly get a new character */
      return;
      };
   if (c == 60) {
      sprintf(s, "&lt;");  /* The '<' will screw up HTML */
      return;
      };
   if (c == 62) {
      sprintf(s, "&gt;");  /* The '>' will screw up HTML */
      return;
      };
   sprintf(s,"%c",c);      /* Write the character(s) to the string */
   } 
