# Makefile for png2html, (c) 1999 Geoff Holden

CC      = gcc
CFLAGS  = -O2 -Wall -lpng -lgd

all:    png2html

png2html:
	$(CC) -o png2html png2html.c $(CFLAGS)

clean:
	rm -f *~ png2html

